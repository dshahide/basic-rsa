def mod_exp(base, exp, mod)
   result = 1 
   base = base % mod
   until exp.zero?
     exp.odd? and result = (result * base) % mod
     exp >>= 1
     base = (base * base) % mod
   end 

   result 
end
