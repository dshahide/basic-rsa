require_relative 'modexp'

def get_sd(prime)
  s = 0
  d = prime-1
 
  while d % 2 == 0 
    d /= 2
    s += 1
  end

  return [s,d]
end

def miller_rabin_pass(s, d, n) 
    a = Random.new.rand(2..(n-2)) 
    x = mod_exp(a,d,n) 
    if x == 1 or x == (n-1) then return true end 
    for r in 1..(s-1) do
      x = (x**2) % n
      if x == 1 then return false end
      if x == (n-1) then return true end 
    end

    false
end

def find_prime(prime)
  if prime <= 1 then return false end 
  if prime <= 3 then return true end
  if prime % 2 == 0 then return false end 
    
  s, d = get_sd(prime)
  result = true

  for i in 0..1000 do
    result = miller_rabin_pass(s, d, prime)
  end

  result 
end 

puts find_prime(Integer(ARGV[0]))
