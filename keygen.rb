def get_co_prime(phin)
  e = 2
  while e < phin
    if phin % e != 0 then 
      if gcd(phin, e) == 1 then return e end
    end
    e += 1
  end 
end

def gcd(a, b)
	while b != 0  
    t = b
    b = a % b
    a = t
  end
  a
end

def extended_gcd(a,b)
  return [0,1] if a % b == 0
  x,y = extended_gcd(b, a%b)
  [y, x-y*(a/b)]
end


def get_priv_key(e, phin)
  d,y = extended_gcd(e, phin)
  d += phin if d < 0 
  d
end

def generate_keys(p, q)
  n = p*q 
  phin = (p-1)*(q-1)
  e = get_co_prime(phin)
  d = get_priv_key(e, phin)

  puts "Public Key: (#{n}, #{e})"
  puts "Private Key: (#{n}, #{d})" 
end

p = Integer(ARGV[0])
q = Integer(ARGV[1])
generate_keys(p,q)
