require_relative 'modexp'

def gen_number(nbits)  
  result = 0
  (nbits-1).downto(0){ |i|
    bit = Random.rand(2) << i
    result = result|bit
  }
  a = (1 << (nbits-1)) + 1
  result = result|a
end

def check_prime(num)
  1000.times{
    a = Random.rand(1..(num-1))
    if mod_exp(a, (num-1), num) != 1 then return false end
  }

  return true
end

def generate_prime(bits)
  if bits <= 0 then 
    puts "Invalid number of bits."
    return 
  end 
  
  result = false 
  
  while !result
    num = gen_number(bits)
    if check_prime(num) then result = num end
  end
  
  puts result 
end

generate_prime(Integer(ARGV[0]))
