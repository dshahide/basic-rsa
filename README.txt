#############################################
################ Notes ######################
#############################################

1. All of the code implementations are in Ruby. Therefore, there are no makefiles or any sort of shell scripts to compile.The source code can be viewed and run from the src folder.
2. Make sure you have the a current version of Ruby installed on your system. All of the code was developed with the following ruby version: ruby 1.9.3p194 (2012-04-20 revision 35410)
3. modexp.rb is contained in the src file. It is a segment of code that performs modular exponenation, an essential process when implementing the RSA algorithm. Since this portion of code in most of the executed files, it was extracted so it could be referenced by all the other files. 

**On my machine generating a 1024-bit prime takes about 7.5 seconds, on average. Checking the number with primecheck.rb takes about 6.4 seconds. This is because the prime is checking using the Miller-Rabin Primality Test. To be sure that I was truly generating a prime, I ran a large a number of "check tests" which definitely increases the output time. 

#############################################
########### To Run Primegen #################
#############################################

ruby primegen.rb [numberofBits]

Example Run: 

$ ruby primegen.rb 10 
$ 821

#############################################
########### To Run Primecheck ###############
#############################################

ruby primecheck.rb [numberToCheck]

Example Run:

$ ruby primecheck.rb 821
$ true

#############################################
########### To Run KeyGen ###################
#############################################

ruby keygen.rb [p] [q]

Example Run:

$ ruby keygen.rb 127 131 
$ Public Key: (16637, 11)
$ Private Key: (16637, 14891)

#############################################
########### To Run Encrypt ##################
#############################################

ruby encrypt.rb [n] [e] [c]

Example Run:

$ ruby encrypt.rb 16637 11 20 
$ 12046

#############################################
########### To Run Decrypt ##################
#############################################

ruby decrypt.rb [n] [d] [encrypted character]

Example Run:

$ ruby decrypt.rb 16637 14891 12046
$ 20 
